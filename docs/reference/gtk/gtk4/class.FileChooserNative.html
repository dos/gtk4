<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!--
SPDX-FileCopyrightText: 2021 GNOME Foundation

SPDX-License-Identifier: Apache-2.0 OR GPL-3.0-or-later
-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Gtk.FileChooserNative</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="utf-8" />

  
  <meta property="og:type" content="website"/>

  
  <meta property="og:image:width" content="256"/>
  <meta property="og:image:height" content="256"/>
  <meta property="og:image:secure_url" content="gtk-logo.svg"/>
  <meta property="og:image:alt" content="Gtk-4.0"/>
  

  
  <meta property="og:title" content="Gtk.FileChooserNative"/>
  <meta property="og:description" content="Reference for Gtk.FileChooserNative"/>
  <meta name="twitter:title" content="Gtk.FileChooserNative"/>
  <meta name="twitter:description" content="Reference for Gtk.FileChooserNative"/>


  
  <meta name="twitter:card" content="summary"/>

  

  <link rel="stylesheet" href="style.css" type="text/css" />

  

  
  <script src="urlmap.js"></script>
  
  
  <script src="fzy.js"></script>
  <script src="search.js"></script>
  
  <script src="main.js"></script>
  <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
  <div id="body-wrapper" tabindex="-1">

    <nav class="sidebar devhelp-hidden">
      
      <div class="section">
        <img src="gtk-logo.svg" class="logo"/>
      </div>
      
      
      <div class="search section">
        <form id="search-form" autocomplete="off">
          <input id="search-input" type="text" name="do-not-autocomplete" placeholder="Click, or press 's' to search" autocomplete="off"/>
        </form>
      </div>
      
      <div class="section namespace">
        <h3><a href="index.html">Gtk</a></h3>
        <p>API Version: 4.0</p>
        
        <p>Library Version: 4.3.1</p>
        
      </div>
      
<div class="section">
  <h5>Type</h5>
  <div class="links">
    <a class="current" href="class.FileChooserNative.html#description">FileChooserNative</a>
  </div>
</div>


<div class="section">
  <h5>Constructors</h5>
  <div class="links">
  
    <a class="ctor" href="ctor.FileChooserNative.new.html">new</a>
  
  </div>
</div>



<div class="section">
  <h5>Instance methods</h5>
  <div class="links">
  
    <a class="method" href="method.FileChooserNative.get_accept_label.html">get_accept_label</a>
  
    <a class="method" href="method.FileChooserNative.get_cancel_label.html">get_cancel_label</a>
  
    <a class="method" href="method.FileChooserNative.set_accept_label.html">set_accept_label</a>
  
    <a class="method" href="method.FileChooserNative.set_cancel_label.html">set_cancel_label</a>
  
  </div>
</div>



<div class="section">
  <h5>Properties</h5>
  <div class="links">
  
    <a class="property" href="property.FileChooserNative.accept-label.html">accept-label</a>
  
    <a class="property" href="property.FileChooserNative.cancel-label.html">cancel-label</a>
  
  </div>
</div>











    </nav>

    
    <button id="btn-to-top" class="hidden"><span class="up-arrow"></span></button>
    

    
<section id="main" class="content">
  <header>
    <h3>Class</h3>
    <h1 aria-label="Name"><a href="index.html">Gtk</a><span class="sep" role="presentation"></span>FileChooserNative</h1>
  </header>

  <section>
    <summary>
      <div class="toggle-wrapper">
        <h4 id="description" style="display:flex;">
          Description
          <a href="#description" class="anchor"></a>
          
          <a class="srclink" title="go to source location" href="https://gitlab.gnome.org/GNOME/gtk/-/blob/master/gtk/gtkfilechoosernative.c#L40">[src]</a>
          
        </h4>

        <pre><code>final class Gtk.FileChooserNative : Gtk.NativeDialog {
  /* No available fields */
}</pre></code>

        <div class="docblock">
          <p><code>GtkFileChooserNative</code> is an abstraction of a dialog suitable
for use with “File Open” or “File Save as”&nbsp;commands.</p>
<p>By default, this just uses a <code>GtkFileChooserDialog</code> to implement
the actual dialog. However, on some platforms, such as Windows and
macOS, the native platform file chooser is used instead. When the
application is running in a sandboxed environment without direct
filesystem access (such as Flatpak), <code>GtkFileChooserNative</code> may call
the proper APIs (portals) to let the user choose a file and make it
available to the&nbsp;application.</p>
<p>While the <span class="caps">API</span> of <code>GtkFileChooserNative</code> closely mirrors <code>GtkFileChooserDialog</code>,
the main difference is that there is no access to any <code>GtkWindow</code> or <code>GtkWidget</code>
for the dialog. This is required, as there may not be one in the case of a
platform native&nbsp;dialog.</p>
<p>Showing, hiding and running the dialog is handled by the
<a href="class.NativeDialog.html"><code>GtkNativeDialog</code></a>&nbsp;functions.</p>
<p>Note that unlike <code>GtkFileChooserDialog</code>, <code>GtkFileChooserNative</code> objects
are not toplevel widgets, and <span class="caps">GTK</span> does not keep them alive. It is your
responsibility to keep a reference until you are done with the&nbsp;object.</p>
<h2 id="typical-usage">Typical usage<a class="md-anchor" href="#typical-usage" title="Permanent link"></a></h2>
<p>In the simplest of cases, you can the following code to use
<code>GtkFileChooserNative</code> to select a file for&nbsp;opening:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span> <span class="kt">void</span>
<span class="n">on_response</span> <span class="p">(</span><span class="n">GtkNativeDialog</span> <span class="o">*</span><span class="n">native</span><span class="p">,</span>
             <span class="kt">int</span>              <span class="n">response</span><span class="p">)</span>
<span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="n">response</span> <span class="o">==</span> <span class="n">GTK_RESPONSE_ACCEPT</span><span class="p">)</span>
    <span class="p">{</span>
      <span class="n">GtkFileChooser</span> <span class="o">*</span><span class="n">chooser</span> <span class="o">=</span> <span class="n">GTK_FILE_CHOOSER</span> <span class="p">(</span><span class="n">native</span><span class="p">);</span>
      <span class="n">GFile</span> <span class="o">*</span><span class="n">file</span> <span class="o">=</span> <span class="n">gtk_file_chooser_get_file</span> <span class="p">(</span><span class="n">chooser</span><span class="p">);</span>

      <span class="n">open_file</span> <span class="p">(</span><span class="n">file</span><span class="p">);</span>

      <span class="n">g_object_unref</span> <span class="p">(</span><span class="n">file</span><span class="p">);</span>
    <span class="p">}</span>

  <span class="n">g_object_unref</span> <span class="p">(</span><span class="n">native</span><span class="p">);</span>
<span class="p">}</span>

  <span class="c1">// ...</span>
  <span class="n">GtkFileChooserNative</span> <span class="o">*</span><span class="n">native</span><span class="p">;</span>
  <span class="n">GtkFileChooserAction</span> <span class="n">action</span> <span class="o">=</span> <span class="n">GTK_FILE_CHOOSER_ACTION_OPEN</span><span class="p">;</span>

  <span class="n">native</span> <span class="o">=</span> <span class="n">gtk_file_chooser_native_new</span> <span class="p">(</span><span class="s">&quot;Open File&quot;</span><span class="p">,</span>
                                        <span class="n">parent_window</span><span class="p">,</span>
                                        <span class="n">action</span><span class="p">,</span>
                                        <span class="s">&quot;_Open&quot;</span><span class="p">,</span>
                                        <span class="s">&quot;_Cancel&quot;</span><span class="p">);</span>

  <span class="n">g_signal_connect</span> <span class="p">(</span><span class="n">native</span><span class="p">,</span> <span class="s">&quot;response&quot;</span><span class="p">,</span> <span class="n">G_CALLBACK</span> <span class="p">(</span><span class="n">on_response</span><span class="p">),</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">gtk_native_dialog_show</span> <span class="p">(</span><span class="n">GTK_NATIVE_DIALOG</span> <span class="p">(</span><span class="n">native</span><span class="p">));</span>
</code></pre></div>

<p>To use a <code>GtkFileChooserNative</code> for saving, you can use&nbsp;this:</p>
<div class="codehilite"><pre><span></span><code><span class="k">static</span> <span class="kt">void</span>
<span class="n">on_response</span> <span class="p">(</span><span class="n">GtkNativeDialog</span> <span class="o">*</span><span class="n">native</span><span class="p">,</span>
             <span class="kt">int</span>              <span class="n">response</span><span class="p">)</span>
<span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="n">response</span> <span class="o">==</span> <span class="n">GTK_RESPONSE_ACCEPT</span><span class="p">)</span>
    <span class="p">{</span>
      <span class="n">GtkFileChooser</span> <span class="o">*</span><span class="n">chooser</span> <span class="o">=</span> <span class="n">GTK_FILE_CHOOSER</span> <span class="p">(</span><span class="n">native</span><span class="p">);</span>
      <span class="n">GFile</span> <span class="o">*</span><span class="n">file</span> <span class="o">=</span> <span class="n">gtk_file_chooser_get_file</span> <span class="p">(</span><span class="n">chooser</span><span class="p">);</span>

      <span class="n">save_to_file</span> <span class="p">(</span><span class="n">file</span><span class="p">);</span>

      <span class="n">g_object_unref</span> <span class="p">(</span><span class="n">file</span><span class="p">);</span>
    <span class="p">}</span>

  <span class="n">g_object_unref</span> <span class="p">(</span><span class="n">native</span><span class="p">);</span>
<span class="p">}</span>

  <span class="c1">// ...</span>
  <span class="n">GtkFileChooserNative</span> <span class="o">*</span><span class="n">native</span><span class="p">;</span>
  <span class="n">GtkFileChooser</span> <span class="o">*</span><span class="n">chooser</span><span class="p">;</span>
  <span class="n">GtkFileChooserAction</span> <span class="n">action</span> <span class="o">=</span> <span class="n">GTK_FILE_CHOOSER_ACTION_SAVE</span><span class="p">;</span>

  <span class="n">native</span> <span class="o">=</span> <span class="n">gtk_file_chooser_native_new</span> <span class="p">(</span><span class="s">&quot;Save File&quot;</span><span class="p">,</span>
                                        <span class="n">parent_window</span><span class="p">,</span>
                                        <span class="n">action</span><span class="p">,</span>
                                        <span class="s">&quot;_Save&quot;</span><span class="p">,</span>
                                        <span class="s">&quot;_Cancel&quot;</span><span class="p">);</span>
  <span class="n">chooser</span> <span class="o">=</span> <span class="n">GTK_FILE_CHOOSER</span> <span class="p">(</span><span class="n">native</span><span class="p">);</span>

  <span class="k">if</span> <span class="p">(</span><span class="n">user_edited_a_new_document</span><span class="p">)</span>
    <span class="n">gtk_file_chooser_set_current_name</span> <span class="p">(</span><span class="n">chooser</span><span class="p">,</span> <span class="n">_</span><span class="p">(</span><span class="s">&quot;Untitled document&quot;</span><span class="p">));</span>
  <span class="k">else</span>
    <span class="nf">gtk_file_chooser_set_file</span> <span class="p">(</span><span class="n">chooser</span><span class="p">,</span> <span class="n">existing_file</span><span class="p">,</span> <span class="nb">NULL</span><span class="p">);</span>

  <span class="n">g_signal_connect</span> <span class="p">(</span><span class="n">native</span><span class="p">,</span> <span class="s">&quot;response&quot;</span><span class="p">,</span> <span class="n">G_CALLBACK</span> <span class="p">(</span><span class="n">on_response</span><span class="p">),</span> <span class="nb">NULL</span><span class="p">);</span>
  <span class="n">gtk_native_dialog_show</span> <span class="p">(</span><span class="n">GTK_NATIVE_DIALOG</span> <span class="p">(</span><span class="n">native</span><span class="p">));</span>
</code></pre></div>

<p>For more information on how to best set up a file dialog,
see the <a href="class.FileChooserDialog.html"><code>GtkFileChooserDialog</code></a>&nbsp;documentation.</p>
<h2 id="response-codes">Response Codes<a class="md-anchor" href="#response-codes" title="Permanent link"></a></h2>
<p><code>GtkFileChooserNative</code> inherits from <a href="class.NativeDialog.html"><code>GtkNativeDialog</code></a>,
which means it will return <code>GTK_RESPONSE_ACCEPT</code> if the user accepted,
and <code>GTK_RESPONSE_CANCEL</code> if he pressed cancel. It can also return
<code>GTK_RESPONSE_DELETE_EVENT</code> if the window was unexpectedly&nbsp;closed.</p>
<h2 id="differences-from-gtkfilechooserdialog">Differences from <code>GtkFileChooserDialog</code><a class="md-anchor" href="#differences-from-gtkfilechooserdialog" title="Permanent link"></a></h2>
<p>There are a few things in the <a href="iface.FileChooser.html"><code>GtkFileChooser</code></a> interface that
are not possible to use with <code>GtkFileChooserNative</code>, as such use would
prohibit the use of a native&nbsp;dialog.</p>
<p>No operations that change the dialog work while the dialog is visible.
Set all the properties that are required before showing the&nbsp;dialog.</p>
<h2 id="win32-details">Win32 details<a class="md-anchor" href="#win32-details" title="Permanent link"></a></h2>
<p>On windows the <code>IFileDialog</code> implementation (added in Windows Vista) is
used. It supports many of the features that <code>GtkFileChooser</code> has, but
there are some things it does not&nbsp;handle:</p>
<ul>
<li>Any <a href="class.FileFilter.html"><code>GtkFileFilter</code></a> added using a&nbsp;mimetype</li>
</ul>
<p>If any of these features are used the regular <code>GtkFileChooserDialog</code>
will be used in place of the native&nbsp;one.</p>
<h2 id="portal-details">Portal details<a class="md-anchor" href="#portal-details" title="Permanent link"></a></h2>
<p>When the <code>org.freedesktop.portal.FileChooser</code> portal is available on
the session bus, it is used to bring up an out-of-process file chooser.
Depending on the kind of session the application is running in, this may
or may not be a <span class="caps">GTK</span> file&nbsp;chooser.</p>
<h2 id="macos-details">macOS details<a class="md-anchor" href="#macos-details" title="Permanent link"></a></h2>
<p>On macOS the <code>NSSavePanel</code> and <code>NSOpenPanel</code> classes are used to provide
native file chooser dialogs. Some features provided by <code>GtkFileChooser</code>
are not&nbsp;supported:</p>
<ul>
<li>Shortcut&nbsp;folders.</li>
</ul>
        </div>

        <div class="docblock">
          <table class="attributes">
            
            
            
          </table>
        </div>

        
      </div>
    </summary>

    
    <div class="toggle-wrapper hierarchy">
      <h4 id="hierarchy">
        Hierarchy
        <a href="#hierarchy" class="anchor"></a>
      </h4>
      <div class="docblock" alt="Hierarchy for Gtk.FileChooserNative">
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
 "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<!-- Generated by graphviz version 2.44.0 (0)
 -->
<!-- Title: hierarchy Pages: 1 -->
<svg width="172pt" height="260pt"
 viewBox="0.00 0.00 172.00 260.00" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="graph0" class="graph" transform="scale(1 1) rotate(0) translate(4 256)">
<title>hierarchy</title>
<!-- this -->
<g id="node1" class="node">
<title>this</title>
<g id="a_node1"><a xlink:title="GtkFileChooserNative">
<path fill="none" stroke="black" d="M152,-108C152,-108 12,-108 12,-108 6,-108 0,-102 0,-96 0,-96 0,-84 0,-84 0,-78 6,-72 12,-72 12,-72 152,-72 152,-72 158,-72 164,-78 164,-84 164,-84 164,-96 164,-96 164,-102 158,-108 152,-108"/>
<text text-anchor="middle" x="82" y="-86.3" font-family="Times-Roman" font-size="14.00">GtkFileChooserNative</text>
</a>
</g>
</g>
<!-- implements_0 -->
<g id="node4" class="node link">
<title>implements_0</title>
<g id="a_node4"><a xlink:href="class.FileChooser.html" xlink:title="GtkFileChooser">
<polygon fill="none" stroke="black" points="142,-36 22,-36 22,0 142,0 142,-36"/>
<text text-anchor="middle" x="82" y="-14.3" font-family="sans-serif" font-size="14.00">GtkFileChooser</text>
</a>
</g>
</g>
<!-- this&#45;&#45;implements_0 -->
<g id="edge3" class="edge">
<title>this&#45;&#45;implements_0</title>
<path fill="none" stroke="black" stroke-dasharray="1,5" d="M82,-71.7C82,-60.85 82,-46.92 82,-36.1"/>
</g>
<!-- ancestor_0 -->
<g id="node2" class="node link">
<title>ancestor_0</title>
<g id="a_node2"><a xlink:href="class.NativeDialog.html" xlink:title="GtkNativeDialog">
<path fill="none" stroke="black" d="M134,-180C134,-180 30,-180 30,-180 24,-180 18,-174 18,-168 18,-168 18,-156 18,-156 18,-150 24,-144 30,-144 30,-144 134,-144 134,-144 140,-144 146,-150 146,-156 146,-156 146,-168 146,-168 146,-174 140,-180 134,-180"/>
<text text-anchor="middle" x="82" y="-158.3" font-family="Times-Roman" font-size="14.00">GtkNativeDialog</text>
</a>
</g>
</g>
<!-- ancestor_0&#45;&#45;this -->
<g id="edge2" class="edge">
<title>ancestor_0&#45;&#45;this</title>
<path fill="none" stroke="black" d="M82,-143.7C82,-132.85 82,-118.92 82,-108.1"/>
</g>
<!-- ancestor_1 -->
<g id="node3" class="node">
<title>ancestor_1</title>
<g id="a_node3"><a xlink:title="GObject.Object">
<path fill="none" stroke="black" d="M106,-252C106,-252 58,-252 58,-252 52,-252 46,-246 46,-240 46,-240 46,-228 46,-228 46,-222 52,-216 58,-216 58,-216 106,-216 106,-216 112,-216 118,-222 118,-228 118,-228 118,-240 118,-240 118,-246 112,-252 106,-252"/>
<text text-anchor="middle" x="82" y="-230.3" font-family="Times-Roman" font-size="14.00">GObject</text>
</a>
</g>
</g>
<!-- ancestor_1&#45;&#45;ancestor_0 -->
<g id="edge1" class="edge">
<title>ancestor_1&#45;&#45;ancestor_0</title>
<path fill="none" stroke="black" d="M82,-215.7C82,-204.85 82,-190.92 82,-180.1"/>
</g>
</g>
</svg>

      </div>
    </div>
    

    
    <div class="toggle-wrapper ancestors">
      <h4 id="ancestors">
        Ancestors
        <a href="#ancestors" class="anchor"></a>
      </h4>

      <div class="docblock">
        <ul>
        
          
          <li class="class"><a href="class.NativeDialog.html" title="NativeDialog">GtkNativeDialog</a></li>
          
        
          
          <li class="class">GObject</a></li>
          
        
        </ul>
      </div>
    </div>
    

    
    <div class="toggle-wrapper implements">
      <h4 id="implements">
        Implements
        <a href="#implements" class="anchor"></a>
      </h4>

      <div class="docblock">
        <ul>
        
          
          <li class="interface"><a href="iface.FileChooser.html" title="FileChooser">GtkFileChooser</a></li>
          
        
        </ul>
      </div>
    </div>
    

    
    <div class="toggle-wrapper constructors">
      <h4 id="constructors">
        Constructors
        <a href="#constructors" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="ctor.FileChooserNative.new.html">gtk_file_chooser_native_new</a></h6>

          <div class="docblock">
            <p>Creates a new <code>GtkFileChooserNative</code>.</p>
          </div>

          
        </div>
      
      </div>
    </div>
    

    
    <div class="toggle-wrapper methods">
      <h4 id="methods">
        Instance methods
        <a href="#methods" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="method.FileChooserNative.get_accept_label.html">gtk_file_chooser_native_get_accept_label</a></h6>
          <div class="docblock">
            <p>Retrieves the custom label text for the accept&nbsp;button.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.FileChooserNative.get_cancel_label.html">gtk_file_chooser_native_get_cancel_label</a></h6>
          <div class="docblock">
            <p>Retrieves the custom label text for the cancel&nbsp;button.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.FileChooserNative.set_accept_label.html">gtk_file_chooser_native_set_accept_label</a></h6>
          <div class="docblock">
            <p>Sets the custom label text for the accept&nbsp;button.</p>
          </div>
          
        </div>
      
        <div class="">
          <h6><a href="method.FileChooserNative.set_cancel_label.html">gtk_file_chooser_native_set_cancel_label</a></h6>
          <div class="docblock">
            <p>Sets the custom label text for the cancel&nbsp;button.</p>
          </div>
          
        </div>
      
      </div>

      
        
        <div class="toggle-wrapper default-hide ancestor-methods">
          <h5 style="display:block;">Methods inherited from <a href="class.NativeDialog.html">GtkNativeDialog</a> (10)</h5>

          
          <div class="docblock">
          
            <h6><a href="method.NativeDialog.destroy.html">gtk_native_dialog_destroy</a></h6>
            <div class="docblock">
              <p>Destroys a&nbsp;dialog.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.get_modal.html">gtk_native_dialog_get_modal</a></h6>
            <div class="docblock">
              <p>Returns whether the dialog is&nbsp;modal.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.get_title.html">gtk_native_dialog_get_title</a></h6>
            <div class="docblock">
              <p>Gets the title of the <code>GtkNativeDialog</code>.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.get_transient_for.html">gtk_native_dialog_get_transient_for</a></h6>
            <div class="docblock">
              <p>Fetches the transient parent for this&nbsp;window.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.get_visible.html">gtk_native_dialog_get_visible</a></h6>
            <div class="docblock">
              <p>Determines whether the dialog is&nbsp;visible.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.hide.html">gtk_native_dialog_hide</a></h6>
            <div class="docblock">
              <p>Hides the dialog if it is visible, aborting any&nbsp;interaction.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.set_modal.html">gtk_native_dialog_set_modal</a></h6>
            <div class="docblock">
              <p>Sets a dialog modal or&nbsp;non-modal.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.set_title.html">gtk_native_dialog_set_title</a></h6>
            <div class="docblock">
              <p>Sets the title of the <code>GtkNativeDialog.</code>.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.set_transient_for.html">gtk_native_dialog_set_transient_for</a></h6>
            <div class="docblock">
              <p>Dialog windows should be set transient for the main application
window they were spawned&nbsp;from.</p>
            </div>
            
          
            <h6><a href="method.NativeDialog.show.html">gtk_native_dialog_show</a></h6>
            <div class="docblock">
              <p>Shows the dialog on the&nbsp;display.</p>
            </div>
            
          
          </div>
          
        </div>
        
      
        
      

      
        
        <div class=" ancestor-methods">
          <h5 style="display:block;">Methods inherited from <a href="iface.FileChooser.html">GtkFileChooser</a> (25)</h5>

          
        </div>
        
      
    </div>
    

    
    <div class="toggle-wrapper properties">
      <h4 id="properties">
        Properties
        <a href="#properties" class="anchor"></a>
      </h4>

      <div class="docblock">
      
        <div class="">
          <h6><a href="property.FileChooserNative.accept-label.html">Gtk.FileChooserNative:accept-label</a></h6>

          <div class="docblock">
            <p>The text used for the label on the accept button in the dialog, or
<code>NULL</code> to use the default&nbsp;text.</p>
          </div>

          
        </div>
      
        <div class="">
          <h6><a href="property.FileChooserNative.cancel-label.html">Gtk.FileChooserNative:cancel-label</a></h6>

          <div class="docblock">
            <p>The text used for the label on the cancel button in the dialog, or
<code>NULL</code> to use the default&nbsp;text.</p>
          </div>

          
        </div>
      
      </div>

      
        
        <div class="toggle-wrapper default-hide ancestor-properties">
          <h5 style="display:block;">Properties inherited from <a href="class.NativeDialog.html">GtkNativeDialog</a> (4)</h5>

          
          <div class="docblock">
          
            <h6><a href="property.NativeDialog.modal.html">Gtk.NativeDialog:modal</a></h6>
            <div class="docblock">
              <p>Whether the window should be modal with respect to its transient&nbsp;parent.</p>
            </div>
            
          
            <h6><a href="property.NativeDialog.title.html">Gtk.NativeDialog:title</a></h6>
            <div class="docblock">
              <p>The title of the dialog&nbsp;window.</p>
            </div>
            
          
            <h6><a href="property.NativeDialog.transient-for.html">Gtk.NativeDialog:transient-for</a></h6>
            <div class="docblock">
              <p>The transient parent of the dialog, or <code>NULL</code> for&nbsp;none.</p>
            </div>
            
          
            <h6><a href="property.NativeDialog.visible.html">Gtk.NativeDialog:visible</a></h6>
            <div class="docblock">
              <p>Whether the window is currently&nbsp;visible.</p>
            </div>
            
          
          </div>
          
        </div>
        
      
        
      

      
        
        <div class="toggle-wrapper default-hide ancestor-properties">
          <h5 style="display:block;">Properties inherited from <a href="iface.FileChooser.html">GtkFileChooser</a> (6)</h5>

          
          <div class="docblock">
          
            <h6><a href="property.FileChooser.action.html">Gtk.FileChooser:action</a></h6>
            <div class="docblock">
              <p>The type of operation that the file chooser is&nbsp;performing.</p>
            </div>
            
          
            <h6><a href="property.FileChooser.create-folders.html">Gtk.FileChooser:create-folders</a></h6>
            <div class="docblock">
              <p>Whether a file chooser not in <code>GTK_FILE_CHOOSER_ACTION_OPEN</code> mode
will offer the user to create new&nbsp;folders.</p>
            </div>
            
          
            <h6><a href="property.FileChooser.filter.html">Gtk.FileChooser:filter</a></h6>
            <div class="docblock">
              <p>The current filter for selecting files that are&nbsp;displayed.</p>
            </div>
            
          
            <h6><a href="property.FileChooser.filters.html">Gtk.FileChooser:filters</a></h6>
            <div class="docblock">
              <p>A <code>GListModel</code> containing the filters that have been
added with&nbsp;gtk_file_chooser_add_filter().</p>
            </div>
            
          
            <h6><a href="property.FileChooser.select-multiple.html">Gtk.FileChooser:select-multiple</a></h6>
            <div class="docblock">
              <p>Whether to allow multiple files to be&nbsp;selected.</p>
            </div>
            
          
            <h6><a href="property.FileChooser.shortcut-folders.html">Gtk.FileChooser:shortcut-folders</a></h6>
            <div class="docblock">
              <p>A <code>GListModel</code> containing the shortcut folders that have been
added with&nbsp;gtk_file_chooser_add_shortcut_folder().</p>
            </div>
            
          
          </div>
          
        </div>
        
      

    </div>
    

    

    
    <div class="class toggle-wrapper default-hide">
      <h4 id="class-struct">
        Class structure
        <a href="#class-struct" class="anchor"></a>
      </h4>

      <div class="docblock">
        <pre><code>struct GtkFileChooserNativeClass {
  GtkNativeDialogClass parent_class;
  
}</code></pre>
      </div>

      
      <div class="docblock">
        <h6>Class members</h6>

        <table class="members">
        
        <tr>
          <td style="vertical-align:top"><code>parent_class</code></td>
          <td style="vertical-align:top"><pre><code>GtkNativeDialogClass</code></pre></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td style="vertical-align:top">No description available.</td>
        </td>
        
        </table>
      </div>
      
    </div>
    

    

     

    

  </section>
</section>


    <section id="search" class="content hidden"></section>

    <footer>
    
    </footer>
  </div>
</body>
</html>